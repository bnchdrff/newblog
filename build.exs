Mix.install([:earmark, :sizeable, :slugify, :yaml_front_matter])

defmodule Build do
  def parse_post(path) do
    {:ok, frontmatter, md} = YamlFrontMatter.parse_file(path)
    {:ok, html, []} = Earmark.as_html(md)
    {frontmatter, html}
  end

  def list_posts(dir \\ "posts") do
    with {:ok, subdirs} <- File.ls(dir), do: Enum.map(subdirs, &(dir <> "/" <> &1 <> "/index.md"))
  end

  defp header(title),
    do: ~s"""
    <!DOCTYPE html>
    <html>
    <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <title>#{title}</title>
    </head>
    <body style="max-width:50em">
    <h1><a href="/">detriot.org</a></h1>
    <h2>Benjamin Chodoroff's blog</h2>

    """

  defp footer,
    do: """
    <hr>
    Thanks for reading. Email me at ben at falafelcopter dot com. <a rel="me" href="https://mastodon.sdf.org/@bnchdrff">Read my posts here.</a>
    </body>
    </html>
    """

  def data_to_html({frontmatter, post_html}) do
    header(frontmatter["date"] <> " " <> frontmatter["title"]) <> post_html <> footer()
  end

  def frontmatter_to_path(%{"date" => date, "title" => title} = frontmatter) do
    {:ok, datetime, _} = DateTime.from_iso8601(date)

    date_part =
      datetime
      |> DateTime.to_date()
      |> Date.to_string()
      |> String.replace("-", "/")

    title_part =
      case frontmatter["slug"] do
        nil -> Slug.slugify(title)
        slug -> String.trim_leading(slug, "/")
      end

    date_part <> "/" <> title_part
  end

  def md_to_html(md_path, build_prefix) do
    {frontmatter, html} =
      md_path
      |> parse_post

    unprefixed_path = frontmatter_to_path(frontmatter)
    path = build_prefix <> unprefixed_path

    :ok = File.mkdir_p(path)

    templated = data_to_html({frontmatter, html})

    html_path = path <> "/index.html"

    :ok = File.write(html_path, templated)

    Map.merge(frontmatter, %{
      "path" => unprefixed_path,
      "size" => Sizeable.filesize(byte_size(templated))
    })
  end

  def index(metas, build_prefix) do
    thead = """
    <table>
    <tr><th valign="top"><img src="/icons/blank.gif" alt="[ICO]"></th><th><a href="#">Name</a></th><th><a href="#">Last modified</a></th><th><a href="#">Size</a></th><th><a href="#">Description</a></th></tr>
    <tr><th colspan="5"><hr></th></tr>
    <tr><td valign="top"><img src="/icons/back.gif" alt="[PARENTDIR]"></td><td><a href="/">Parent Directory</a></td><td>&nbsp;</td><td align="right">  - </td><td>&nbsp;</td></tr>

    """

    tfoot = """
    <tr><th colspan="5"><hr></th></tr>
    </table>
    """

    icons = File.ls!("icons")

    # Static seed, so icons don't change too much
    :rand.seed(:exrop, {101, 102, 103})

    rows =
      metas
      |> Enum.map(fn %{"date" => date, "path" => path, "size" => size, "title" => title} = meta ->
        "<tr><td valign='top'><img src='/icons/" <>
          Enum.random(icons) <>
          "' alt='[BLOG POST]'></td><td><a href='/" <>
          path <>
          "'>" <>
          title <>
          "</a></td><td align='right'>" <>
          date <>
          "</td><td align='right'>" <>
          size <> "</td><td>" <> Map.get(meta, "description", "&nbsp;") <> "</td></tr>"
      end)
      |> Enum.reverse()
      |> Enum.join("\n")

    list = thead <> rows <> tfoot

    doc = header("Blog") <> list <> footer()

    File.write(build_prefix <> "/index.html", doc)
  end

  def cp_icons(build_prefix) do
    dest = build_prefix <> "icons"
    File.mkdir_p!(dest)
    File.cp_r!("./icons", dest)
  end

  def main() do
    build_prefix = "build/"

    {:ok, _} = File.rm_rf(build_prefix)

    cp_icons(build_prefix)

    metas =
      list_posts()
      |> Enum.map(&md_to_html(&1, build_prefix))
      |> Enum.filter(&(!&1["draft"]))
      # Ascending, so random icons don't change each time a new post appears
      |> Enum.sort_by(& &1["date"], :asc)

    index(metas, build_prefix)
  end
end

Build.main()
