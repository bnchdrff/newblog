---
title: "Debian lenny and Xen 3.2.1 on amd64"
slug: "/debian-lenny-and-xen-321-on-amd64"
date: "2009-03-11T16:12:38.000Z"
featured: false
draft: false
tags: ["Tech"]
---

I'm setting up a new server to run a few xen instances, and ran across a few issues:
http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=502798
http://www.mail-archive.com/debian-kernel@lists.debian.org/msg42367.html

1) add an hvc0 entry in inittab and move over other tty's:
1:2345:respawn:/sbin/getty 38400 hvc0
2:23:respawn:/sbin/getty 38400 tty1
and change 2,3,4,5,6 to 3,4,5,6 etc

2) modify system clock settings as in http://wiki.debian.org/Xen clocksource section
