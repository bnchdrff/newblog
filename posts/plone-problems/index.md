---
title: "plone problems"
slug: "/plone-problems"
date: "2009-03-17T02:16:36.000Z"
featured: false
draft: false
tags: ["Tech","plone"]
---

if yr plone instance can't find PIL, and you're sure it's installed, maybe it's because you have to create a link from the PIL egg to "PIL"

also, if plone's UI doesn't work at all, maybe it's because jquery was removed from the js registry and you have to go back and add it!
