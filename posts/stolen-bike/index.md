---
title: "Stolen bike"
slug: "/stolen-bike"
date: "2010-08-25T17:53:05.000Z"
featured: false
draft: false
tags: ["Bicycles","orange bike","thievery","wahwahwah"]
---

My bicycle was just stolen from the WSU farmers market today, around noon.

Details:
* large orange Nashbar mountain bike frame, only decal is a "nashbar" signature on outside of left chainstay
* black rigid fork
* dt/hugi hubs, avid mechanical disc brakes, mismatched rims
* slick tires: specialized armadillos
* titec bar/stem/post
* sram 9.0 rear derailleur, shimano front
* green raceface cranks

<a href="https://old.detriot.org/wp-content/uploads/2010/08/bens-orange-bike.jpg"><img src="https://old.detriot.org/wp-content/uploads/2010/08/bens-orange-bike-300x225.jpg" alt="" title="me + the orange bike at franklin fountain... weird right?" width="300" height="225" class="alignleft size-medium wp-image-213" /></a>
