---
title: "Trackpoint keyboards"
slug: "/trackpoint-keyboards"
date: "2014-03-13T20:47:15.000Z"
featured: false
draft: false
tags: []
---

After using an IBM T30 years ago, I've become accustomed to having a pointing device between my G-H-B. And until noticing a strange nub on my friend's HP notebook, I had assumed that only Thnkpads had them!

In fear of bulletin board decay, I'm reposting a list that I found on [hp's support forums](http://h30434.www3.hp.com/t5/Notebook-Hardware/Which-new-notebook-models-have-trackpoint/td-p/665249):

```
Machines, which I`m aware of, that currently offer trackpoints are:
All EliteBooks; ProBook 6450b, 6455b & 6550b.
All known ThinkPads (not IdeaPads).
NEC EasyNote MX45, MX65, S5.
Fujitsu Lifebook T2020, P1630, P2120, S7220, E8420, U820/U2010.
Dell Latitude E4200, E4310, E6410, E6420 and E6510; Precision M4500 and M6500.
Toshiba Tecra R840, R850, M11, A11 (optional).
Acer TravelMate C200, C210, 6410, 6460, 6492, 6492G, 6592, 6592G, 6593.

Machines, which I`m aware of, that were peviously equipped with trackpoints are:
All HP EliteBooks; all models ending with p or w; all models starting with nc or nw; 6445b (optional), 6545b (optional), tc4200, tc4400.
Sony Vaio P series, BX series, C1 series, U8 series, UX series.
Dell Latitude E6500, E6400, D430, D600, D630, D830, XT, E4300, E5400, E5410, E6400, E6510, E6400 ATG, E6500; Precision M2300, M2400, M4300, M4400, M6400; Inspiron 4000, 8100, 8200, 8600, 9100; L.
Toshiba Portege 3490CT, Tecra A7, A8, A9, A10, M2, M5, M9, M10, S Series; Satellite Pro 4000 Series, 410 Series.
```
