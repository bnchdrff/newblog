---
title: "ec2 security"
slug: "/ec2-security"
date: "2008-12-20T19:50:39.000Z"
featured: false
draft: false
tags: ["Tech"]
---

I'm just getting started with amazon's virtualized environment / web services after having used a few other similar services.  Working on developing a <a href="http://deliverance.openplans.org/">Deliverance</a>-based content proxy to introduce some dynamic content into a large website network.  More on that later.  Following are some links related to ec2 security:

<a href="http://developer.amazonwebservices.com/connect/entry!default.jspa?categoryID=152&amp;externalID=1697&amp;fromSearchPage=true">Amazon's summary of security in its webservices.</a>

<a href="http://techbuddha.wordpress.com/2008/12/10/amazon-aws-securitywhat-a-cloudy-web-we-weave/">One of many articles on ec2 security.</a>
