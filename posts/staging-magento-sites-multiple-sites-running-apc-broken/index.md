---
title: "Staging magento sites: multiple sites running APC == broken"
slug: "/staging-magento-sites-multiple-sites-running-apc-broken"
date: "2010-09-02T15:42:33.000Z"
featured: false
draft: false
tags: ["Uncategorized","magento","nginx","php","sysadmin","webdev"]
---

Well, it's been a wild webhosting ride recently.  I've been automating dev/staging/live production procedures with my Drupal and Magento clients.  In a few days I'll publish a comprehensive blogpost about this whole project, replete with nginx, php, php-fpm, etc configuration goodies.

For now, here's one tidbit that I just can't hold back:

I ran into an interesting problem with running multiple Magento sites on the same server: the new Magento devsite I set up would unfailingly read the old site's db configuration information (from app/etc/local.xml) 9/10 times after restarting nginx and php5-fpm.  And to make things even more confusing, on some restarts, it'd read the devsite db configuration and so would the live site!

After trying every single possible iteration of php5-fpm and nginx options that I thought might be relevant, I remembered that I had set up Magento to use APC for caching, and changed the prefix:

<pre lang="xml" line="1">
<config>
  <global> 
<!-- ... -->
        <cache>
            <backend>apc</backend>
            <prefix>alphanumeric (i.e. change this to something else)</prefix>
        </cache>
<!-- ... -->
     </global>
  </config>
</pre>

It took a LONG time to track down this issue.  I had forgotten that I had apc cache running on this site, and I never would've guessed that it would mix up different site's caches.  The search was even more complicated because I am not as confident with my nginx/php-fpm skills as i am with apache/mod_php.  Serves me right for blindly implementing the magento apc cache without understanding what the various xml config options mean.

After finding the problem on my own, I searched to see if anyone else has experienced this and found a <a href="http://www.magentocommerce.com/boards/viewreply/51878/">post on the magento board</a>.  Wish I'd seen that 6 hours ago.

I'm going to add an APC-prefix-changing sed operation to my magento dev/staging/production scripts now.
