---
title: "moving a plone site from server to server + notes on virtualization"
slug: "/moving-a-plone-site-from-server-to-server-notes-on-virtualization"
date: "2009-02-14T01:36:25.000Z"
featured: false
draft: false
tags: ["Tech","plone","sysadmin","zope"]
---

After tiring of a slow Zen VPS provider, I've moved <a href="http://www.hamtramckcitizen.com/">The Citizen Newspaper of Hamtramck's site</a> to some big iron (dual Xeon 64bit).  To accomplish this, I simply mirrored the zope file structure (nicely contained in a virtualenv), sync'ed up the Data.fs, started a zeoserver on the target server using that Data.fs, switched the instance on the old server to use that zeoserver (so both servers use the same content during dns changeover), started up an instance on the new server, and changed DNS.

So fresh!

In any case, I'm growing tired of other people's virtual servers.. especially the growing expense. Gandi's introductory rates were nice, but they've raised them to beyond market value (compared to EC2/Gogrid at least).  I'm looking into setting up a vps system of my own and colocating it.
