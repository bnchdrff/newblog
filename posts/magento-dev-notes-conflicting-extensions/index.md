---
title: "Magento dev notes: conflicting extensions"
slug: "/magento-dev-notes-conflicting-extensions"
date: "2011-03-01T13:48:59.000Z"
featured: false
draft: false
tags: ["Tech","magento","php","programming","webdev"]
---

Scenario: you've developed a magento extension, tested it on your development server for all these potential fringe cases, even brought in live customer data+code to test against... finally you deploy it to the live site, only to find that another (3rd party) extension uses the same dirty &lt;rewrite&gt;s as your code does.  Damn.

What a great occasion to learn all about resolving namespace conflicts in magento.  Here's my list of links:

This is really all you need to know: <a href="http://www.webshopapps.com/blog/2010/11/resolving-magento-extension-conflicts/">http://www.webshopapps.com/blog/2010/11/resolving-magento-extension-conflicts/</a>

If you don't know how to use your text editor, you might need to use this extension for help: <a href="http://www.maisondulogiciel.com/english/magento/magento-extension-conflict.html">http://www.maisondulogiciel.com/english/magento/magento-extension-conflict.html</a>

And finally, here's an excellent / slightly confounding exposé: <a href="http://magebase.com/magento-tutorials/magento-extension-clashes-winners-and-loosers/">http://magebase.com/magento-tutorials/magento-extension-clashes-winners-and-loosers/</a>
