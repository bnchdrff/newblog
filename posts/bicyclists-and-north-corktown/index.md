---
title: "bicyclists and North Corktown"
slug: "/bicyclists-and-north-corktown"
date: "2011-02-13T21:46:56.000Z"
featured: false
draft: false
tags: ["Bicycles","detroit","economics","grantiefundie","policy"]
---

Recently a few of my friends have asked me what could help encourage more people in my neighborhood to ride bicycles.  For all the time I've been a bicycle commuter and advocate for cyclists in Detroit, I've never really considered what a specific neighborhood could do to encourage bicycle use.  My work at <a href="http://thehubofdetroit.org/">Back Alley Bikes / The Hub of Detroit</a> is rooted in a neighborhood that I moved out of, and <a href="http://en.wikipedia.org/wiki/Jeffries_Projects#Cornerstone">after the Jeffries projects closed down</a> it turned into less of a neighborhood resource and more of a city-wide resource (more on that in a minute).  Whenever I've seen bicycle advocacy come up in the public discourse, it's been about city-wide or state-wide issues (bicycle licensing, safe streets funding, etc).  It's not often that I see local/block-level efforts to promote green transportation...  but isn't that the right place to start?

[caption id="attachment_264" align="alignleft" width="300" caption="Blair rides her bike when it&#39;s cold"]<img class="size-medium wp-image-264" src="https://old.detriot.org/wp-content/uploads/2011/02/P1010743-300x225.jpg" alt="" width="300" height="225" />[/caption]

It's easy for me to not think about it on the day-to-day:  I'm more than happy to jump on my bike and ride to work 5 miles away in the middle of winter... mostly because I've spent my whole life growing fond of cold-weather discomfort and learning how to fix a flat with frozen fingers.  What would it take to convince someone with less pain tolerance and more concern for their finger's nerve endings?  If you look at North Corktown from a <a href="http://www.ibike.org/engineering/landuse.htm">mainstream bicycle-aware urban planning standpoint</a>, the situation might seem bleak.  There isn't much in place that would encourage non-automotive transportation: bus stops are mysterious, sidewalks come and go, and the nearest bike shop and grocery store are a half-hour walk away, in opposite directions; in other words, you'd better pray that you don't end up with a broken chain and an empty pantry.

The mainstream urban planning fancy-pants plan for a neighborhood like North Corktown might include accoutrement like bicycle lanes, a marketing campaign to encourage alternative transporatation, and maybe a <a href="http://www.portlandmercury.com/images/blogimages/2008/11/19/r_1227136951_pitpat.jpg">spokesthing</a> for good measure.  I think that a lot of these ideas may not be especially relevant to many of Detroit's neighborhoods.

[caption id="attachment_265" align="alignright" width="300" caption="A pile of cycle in my neighborhood"]<img class="size-medium wp-image-265" src="https://old.detriot.org/wp-content/uploads/2011/02/P1010784-300x225.jpg" alt="" width="300" height="225" />[/caption]

Rather than paying <a href="http://www.walkinginfo.org/engineering/roadway-bicycle.cfm">tens of thousands of dollars per mile</a> for <a href="http://cheezburger.com/View/4244506368">silly things like these</a>, I'd prefer to see that money go into Detroit's growing bicycle service economy.  Detroit has a history of creative entrepreneurship, and the last few years have seen a plethora of bicycle-related businesses start up: The Hub's in-school youth education services and repair shop, Wheelhouse Detroit's bicycle tours, <a href="http://detroitgreencycle.com/Detroit_Greencycle/Home.html">Detroit Greencycle curbside recycling pickup</a>, Cass Cafe's food delivery service.

A bicycle-related business grows the local economy and promotes bicycling at the same time.  These groups are putting down fertile soil to grow bigger and better things on in the future.  Those bike lanes take a lot of nutrients to get going, don't they?  Shouldn't we start with the basics?

I live across the street from an apartment complex that was recently bought by a neighborhood nonprofit.  Many residents look forward to using some of the units for community institutions, and I think that it'd be a perfect site for regular bike education workshops.  An organization like <a href="http://fenderbenderdetroit.wordpress.com/">Fender Bender</a> or <a href="http://www.facebook.com/memakerspace">Mt Elliott Makerspace</a> could provide a mobile toolkit and some educators, and my neighbors could try wrenching a bit or maybe learn some winter bike riding skills.

[caption id="attachment_268" align="alignleft" width="300" caption="Let&#39;s jam econo with small-fry bike education on a massive scale"]<img class="size-medium wp-image-268 " title="We Jam Econo" src="https://old.detriot.org/wp-content/uploads/2011/02/We-Jam-Econo-300x300.jpg" alt="" width="300" height="300" />[/caption]

Running a program like this can be done on a shoestring budget (c.f. Back Alley Bike's budget of 4000/year a few years ago), and can have lasting impact as well as serve as a statistical/research base for expanding into new ideas and ventures.  A few other projects (Earthworks on the east side and All Saints Church in SW) are running their own small-scale bike education programs, and I'd love to see more neighborhoods join in.  In Earthworks' case, The Hub sponsored the formation of a shop at the soup kitchen/community farm and it turned into a program on its own over the course of a year.  And I heard that the youth at All Saints want to open up a retail shop...

It's up to us to make these ideas and plans available to any and all neighborhoods in our city.  As the above projects show, we have what it takes to make amazing things happen, without having to wait on municipal decisions or state mandates.
