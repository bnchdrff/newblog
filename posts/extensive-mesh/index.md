---
title: "Extensive mesh"
slug: "/extensive-mesh"
date: "2010-06-28T13:59:06.000Z"
featured: false
draft: false
tags: ["Tech","amc2010","community wireless","detroit","digital justice","mesh","NoCo","openmesh","ussf"]
---

After an invigorating <a href="http://alliedmedia.org">AMC2010</a>, and a very fruitful HOT MESH <a href="http://talk.alliedmedia.org/message-boards/sessions/hot-mesh-wireless-mesh-network-build">conference session</a> and media lab presence, I'm back at my daily work of code wrangling, system adminstering, etc.  But this afternoon I had the chance to extend the North Corktown community wireless network, with <a href="http://www.danblah.com/">Dan</a> from OTI and a few freshly-prepared wireless routers.

<strong>Back story</strong>

I live in North Corktown, a Detroit neighborhood about two miles from downtown.  I used to live up the street and had a cable modem, but when I moved a few blocks away and tried to transfer my account, the cable company refused to install service.  After a struggle, I ended up with a barely-broadband DSL connection.  Many of my neighbors have had similar problems in trying to get internet access.  To solve this, I set up a small network of <a href="http://upload.wikimedia.org/wikipedia/en/4/4e/Self-form-self-heal.gif">meshed routers</a> to repeat my signal down my block.  Also, a neighbor up the street from me was willing to share his connection with nearby residents, so I set him up a mesh router as well.  Hot Mesh was born, and North Corktown got a little bit of free internet.  People started noticing the signal, random cars started hanging out in front of my house, and I got to meet my new neighbors.

<strong>Enter the Spaulding Court</strong>

Spaulding Court is an apartment complex.  I live across the street from it.  After being neglected by an absentee landlord for a decade or so, some neighbors started getting serious with it and are fixing the place up.  Read more about this whole process in a <a href="http://www.detroityes.com/mb/showthread.php?t=5716">Detroit YES! thread</a> or via the official <a href="http://spauldingcourt.com/">Spaulding Court web presence</a>.

The US Social Forum served as a catalyst for Spaulding Court.  It's playing host to a couple dozen tent-dwellers and temporary apartment tenants, as well as one long-term tenant.  Read a <a href="http://littlehouseontheurbanprairie.wordpress.com/2010/06/28/the-tent-cities-of-noco/">slightly-tongue-in-cheek mise-en-scène</a> by my neighbor (and community wireless participant) Patrick.  The project's organizers were interested in providing internet access to the new residents, and I was willing to help.

A few days ago, we deployed a few more nodes and extended the wireless coverage to the west.   The network now solidly covers about three blocks and is served by two residential DSL connections and a cable modem shared between nine wireless mesh nodes.  All of the new infrastructure will stay in place for the neighborhood to benefit from.

<strong>Technical Details</strong>

Take a look at the <a href="https://dashboard.open-mesh.com/overview2.php?id=courters">network map</a> and notice the distance between nodes -- even with a few trees in the way, we have a couple very large gaps between gateways and repeaters.  The node in the middle, codenamed "landyacht" and stowed in the pantry of an Airstream trailer, acts as a repeater that would bridge traffic if either of the gateways had a problem.  Landyacht is often out of service due to power issues.  The other out-of-service node, Courteous, is sitting on my table waiting to receive a specially-crafted XSS payload.

Both Dan and I were surprised at the power of these little 30mW routers: some of them are traversing multiple empty lots.  We'd thought that we would've needed some cantennas, but it wasn't needed.

[update]   The North Corktown network is actually made up of a few separate Open-Mesh networks:  the one linked to above uses another network as a backhaul, and any other network can use it as a backhaul.  This means that anyone can set up their own nodes, with or without an internet connection, and easily extend the network.  These new networks can have completely different settings (i.e. you can set a password for your own personal network, or limit bandwidth, or give it a funny name).

<strong>The Future</strong>

This is my neighborhood's network.  It worked well because I knew quite a few neighbors who were interested, and I was able to school them in mesh networking theory over and over again until they were all comfortable participating.  Many neighborhoods might not have the advantage of a resident community wifi zealot, but it only takes a few hours of playing with the hardware and learning some networking concepts to become a very capable mesh network technician.

I'd like to see the next <a href="http://alliedmedia.org/down_at_the_discotech">DiscoTech</a> happen alongside a mesh network install and computer training /giveaway.  I think there's a nice pile of computers left around after the US Social Forum that ought to be put to good use.
