---
title: "ssh agent forwarding protip"
slug: "/ssh-agent-forwarding-protip"
date: "2011-06-19T15:47:14.000Z"
featured: false
draft: false
tags: ["Tech","git","identity management","screen","ssh"]
---

my source code management / git workflow just got a bit easier:  i've never used ssh agent forwarding before, but now i can just use one key to manage access to different repos instead of having separate keys on all my hosts.

in the past, i'd never really used "ssh -A" because it didn't automagically work with the screen terminal multiplexer... but now, <a href="http://superuser.com/questions/180148/how-do-you-get-screen-to-automatically-connect-to-the-current-ssh-agent-when-re-a">thanks to superuser, it does</a>!
