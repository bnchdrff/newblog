---
title: "problems with post-thumb wordpress plugin"
slug: "/problems-with-post-thumb-wordpress-plugin"
date: "2009-03-27T23:18:00.000Z"
featured: false
draft: false
tags: ["Tech","php","wordpress"]
---

DEFINEs not defined in post-thumb-template-library.php.

solution: add the defines from the regular post-thumb.php
