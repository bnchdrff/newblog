---
title: "Detroit Ledger at #awesummit"
slug: "/detroit-ledger-at-awesummit"
date: "2013-10-21T01:13:57.000Z"
featured: false
draft: false
tags: ["detroit ledger","presentations","awesummit"]
---

Tomorrow I'll be representing [Detroit Ledger](http://detroitledger.org) at the Awesome Summit. [Lucy's blog post](http://www.philanthropy.blogspot.com/2013/10/designing-change-from-data-up.html) has a nice salient description of our panel:

> The act of giving generates a lot of data. Who’s collecting it, and who should have access to it? How do we manage this delicate personal data respectfully but transparently? What does it mean to design social change from the data up?

I'll be posting during/after the conference about any ledger-related awesome revelations.
