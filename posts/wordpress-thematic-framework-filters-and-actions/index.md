---
title: "wordpress / thematic framework filters and actions"
slug: "/wordpress-thematic-framework-filters-and-actions"
date: "2009-12-01T01:22:22.000Z"
featured: false
draft: false
tags: ["Tech","php","wordpress"]
---

I'm building a new content-managed website for <a href="http://congbethshalom.org">Congregation Beth Shalom</a>, a local synagogue, using Wordpress and the Thematic Framework.

While researching slick, thematic ways to only do a filter on non-home pages, I came across <a href="http://themeshaper.com/forums/topic/customize-functions-in-child-themes">this suggestion</a> to add actions to the init action tag.  It makes a lot more sense to use the wp action tag, though, so various wordpress template variables are made available, e.g.

```
function remove_ulcl() {
 remove_filter('wp_page_menu','thematic_add_menuclass');
}

function add_ulcl() {
 if(!is_home()||!is_front_page()) {
 add_filter('wp_page_menu','thematic_add_menuclass');
 }
}

add_action('wp', 'remove_ulcl');
add_action('wp', 'add_ulcl');
```

Isn't that nice?  I'm digging Thematic.  It's so wordpress-y.
