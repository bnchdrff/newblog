---
title: "GSM hijacking + VoIP + community wifi"
slug: "/gsm-hijacking-voip-community-wifi"
date: "2010-08-02T01:10:36.000Z"
featured: false
draft: false
tags: ["Tech","cellphones","community wireless","detroit","digital justice","gsm","mesh","phones","wireless"]
---

<a href="http://news.ycombinator.com/item?id=1565395">Via HN</a>: Chris Paget demonstrated a <a href="http://www.wired.com/threatlevel/2010/07/intercepting-cell-phone-calls/">nifty little GSM hijacking device at Defc</a>on: with a directional antenna and some clever <a href="http://openbts.sourceforge.net/">OpenBTS</a>-based software, he was able to provide an irresistably-strong signal for cellphones, which would switch off call encryption and then trunk the call onto a VoIP network.

Sure, this is awesome for espionage, playing jokes on your neighbors, etc. but how about running an experimental cellphone network that can provide service for not only VoIP handsets but also old GSM phones?  It'd be great fun to build a renegade cell network using throwaway old GSM handsets and cobbled-together VoIP devices / homemade phonebooths.

NB: <a href="http://news.ycombinator.com/item?id=1565819">One commenter on hackernews</a> pointed out the different types of phones that would allow this hijacking.  Good to know.
