---
title: "ISPconfig and chiliproject"
slug: "/ispconfig-and-chiliproject"
date: "2011-10-11T23:07:41.000Z"
featured: false
draft: false
tags: ["Tech","chiliproject","ispconfig","sysadmin"]
---

Some hints for the internet:
* gem install fcgi
* symbolic link CHILIPROJECT_ROOT/public to ~/web for the ispconfig user
* comment out the <IfModule mod_cgi.c> section of .htaccess in CHILIPROJECT_ROOT/public/

and there you have it. easy-to-deploy project management systems for all your friends!
