---
title: "Late summer updates: leaves, chicken races, hacker spaces, garden sensors"
slug: "/late-summer-updates-leaves-chicken-races-hacker-spaces-garden-sensors"
date: "2010-08-23T03:14:56.000Z"
featured: false
draft: false
tags: ["Tech","chickens","community wireless","detroit","digital justice","dogs","froyo","journal","mesh","seasons"]
---

I'd like to share a few stories:

[caption id="attachment_194" align="alignright" width="300" caption="Auto the dog, with leaves"]<img class="size-medium wp-image-194 " title="Autodog in Fall" src="https://old.detriot.org/wp-content/uploads/2010/08/auto-fall-300x200.jpg" alt="Auto the dog, with leaves" width="300" height="200" />[/caption]

<strong>1) Leaves</strong>

For some odd reason a tree in my backyard thinks it's Autumn, turned yellow, and pooed its leaves all over the place.  Here's a photo with my dog Auto.

This leaf event, combined with the end of an endless heat wave, has noticeably  changed my mood.  Time to start storing up nonperishables and fattening up for winter.  Gotta fix my fender-clad bicycle, fortify my insulation, and spend more time outside before detroit winter coma sets in.

[caption id="attachment_195" align="alignleft" width="240" caption="Racers prepare to drop hen"]<a href="http://www.flickr.com/photos/28827952@N06/sets/72157624785593328/"><img class="size-medium wp-image-195 " title="Detroit's first Chicken Race" src="https://old.detriot.org/wp-content/uploads/2010/08/chicken-race-300x225.jpg" alt="A group of chicken racers prepare to release their hens" width="240" height="180" /></a>[/caption]

<strong>2) Chicken races</strong>

My friend Karthik Kavasseri convinced seventeen chicken farmers to let their fastest hens walk around in circles at the Temple Bar a few weeks ago.

Check <a title="a flicks" href="http://www.flickr.com/photos/28827952@N06/sets/72157624785593328/">a Flickr page</a> for my photos, and also <a href="http://lmgtfy.com/?q=detroit+chicken+race">use the google</a> to read other people's reports.  Mr. Todd Scott has a <a href="http://www.allyeargear.com/2010/first-ever-detroit-chicken-races/">lovely summary</a> and <a href="http://www.allyeargear.com/gallery2/2010/Detroit-chicken-races-2010/">additional photos</a>.

[caption id="attachment_192" align="alignright" width="240" caption="Blair Nosan featured her frozen yogurt product at the Chicken Race"]<a href="http://suddenlysauer.com"><img class="size-medium wp-image-192 " title="Suddenly Sauer's Debut" src="https://old.detriot.org/wp-content/uploads/2010/08/suddenly-sauer-debut-300x225.jpg" alt="Blair Nosan featured her frozen yogurt product at the Chicken Race" width="240" height="180" /></a>[/caption]

While I was busy coaxing one of my chickens down a gravelly aisle, my girlfriend Blair Nosan served up some mean frozen-yogurt (fro-yo, if you will) with toppings.  The froyo was met with universal accolades, save for the one or two people who'd never tasted yogurt before.  Her offerings were nicely paired with the renegade food-cart offerings of the Pink Flamingo, which arrived fashionably late.

My upstairs neighbors and I ran one chicken in this event and I think we finished second.  I delivered the chicken to the event by bungee-cording a gigantic dog crate atop my B.O.B. trailer.  Yes, it was copping a backwards lean but the chicken and more importantly all of the straw was contained.  I delighted in riding through the Motor City Casino stretch of Temple with this contraption on display.  Nothing attracts a crowd like a bicycle trailer and a chicken.

[caption id="attachment_193" align="alignleft" width="300" caption="I totally soldered that Garduino"]<a href="https://old.detriot.org/wp-content/uploads/2010/08/garduino-assembled.jpg"><img class="size-medium wp-image-193" title="Garduino, assembled" src="https://old.detriot.org/wp-content/uploads/2010/08/garduino-assembled-300x200.jpg" alt="I totally soldered that Garduino" width="300" height="200" /></a>[/caption]

<strong>3) Hacker spaces and garden sensors</strong>

As I mentioned in an earlier posting, I joined <a href="http://omnicorpdetroit.com">OmniCorpDetroit</a>, a splendid hackety-hacker outfit in Eastern Market.  I'm currently trying to figure out how to construct mesh-networked temperature sensors to deploy in various Detroit greenhouses and hothouses.

Ideally, every greenhouse will have four temperature probes: soil and ambient, indoor and outdoor.  The probes would transmit back to a nearby wireless mesh network node that would operate as a local data server and, when there's a good 'net connection, upload data somewhere safe.  Mesh nodes would also act as community wireless extensions, if they're in a useful location for that.

This work builds on the idea of connecting technology democracy / digital justice work with community gardening / environmental justice principals.  Read more about these ideas in <a href="http://oti.newamerica.net/publications/special/a_community_wireless_mesh_prototype_in_detroit_mi_34925">OTI's Hot Mesh report</a>.

The initial prototyping will take place at the Mt Elliot Makerspace and the Earthworks Garden on the eastern side of our city.

Random thoughts to be expanded at a later date: thermocouple vs thermistor for temperature sensing: may have opportunity to make our own thermocouples.... do cost comparisons... length of cable, power draw? RF concerns with xbee vs 2.4/5.8 networking / lots of thermo sensors?
