---
title: "Dotfile stuff"
date: "2021-04-07T22:11:14.277Z"
featured: false
draft: false
tags: []
description: annotations on my dotfiles
---

Whenever I start pairing with someone new, we just about always end up teaching new things to each other. These little tricks end up being recorded in my dotfiles repo, which is a huge pile of spaghetti that is not meant to be useful for anyone besides me. I've kept it under git revision control since 2012. It is what it is.

But I often want to share little bits of my setup with people - in this post, I'm going to give some of my favorite things in my dotfiles.

## Tools

### Linux

On Ubuntu, here are some neat programs I end up installing on every new system:

```
openssh-server # I'll almost always want to be able to ssh into my own laptop
kitty # This is an amazing terminal emulator. I love it.
vim-nox # A nice baseline vim, has everything I need to support my collection of plugins
htop # Replacement for top
bwm-ng # Easy way to monitor network throughput
gnome-tweak-tool # Lets you remap caps lock to ctrl, among other things
ctags-exuberant # Used by ctrl-p vim plugin
direnv # Automatically instantiate env vars, per directory
tmuxinator # I only just started using tmuxinator to automate tmux setup for each of my projects
# I also keep a running list of dependencies that I need for whatever projects I have to build frequently
build-essential
libssl-dev
automake
autoconf
libncurses5-dev
libwxgtk3.0-gtk3-dev
libgl1-mesa-dev
git
curl
```

### MacOS

On MacOS, I install Homebrew and then do:

```
brew install bash go pinentry-mac coreutils git curl tmux vim java htop bwm-ng mailhog ctags tmuxinator
```

#### Config on MacOS

Display: I always turn off font smoothing, and choose the middle option for "Scaled" - it takes a little more screen real estate, but your eyes don't get as sore.

I could not survive withuot the [haptic touchbar](https://www.haptictouchbar.com) utility - it makes the trackpad click when you hit the escape key.

### Both OSes

Non-apt/homebrew-installable tools include:

* [gopass](https://github.com/gopasspw/gopass), my favorite password manager (on my phone i use the fantastic Password Store app)
* [joplin](https://github.com/laurent22/joplin), a very nice note taking app that also has a nice phone app - syncs via your choice of cloud storage
* [asdf](https://github.com/asdf-vm/asdf) for managing erlang, elixir, nodejs, and yarn versions

For asdf, i just keep a script like this that I copy/paste to run:

```
git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.7.8
asdf plugin-add erlang https://github.com/asdf-vm/asdf-erlang.git
asdf plugin-add elixir https://github.com/asdf-vm/asdf-elixir.git
asdf install erlang 21.3.8.9
asdf local erlang 21.3.8.9
asdf install elixir 1.10
asdf local elixir 1.10

#  Nodejs stuff
asdf plugin add nodejs
bash -c '${ASDF_DATA_DIR:=$HOME/.asdf}/plugins/nodejs/bin/import-release-team-keyring'
asdf install nodejs
asdf plugin-add yarn
asdf install yarn
yarn config set prefix ~/.yarn # ensures programs added with `yarn global add` will be available in PATH
```

## Vim

I use vim as a text editor. For plugin management, I use [pathogen](https://github.com/tpope/vim-pathogen) because it works just fine. I add new plugins as subtrees in my dotfiles repo.

Here are my plugins, replete with some salesforce and PHP-related relics:

```
alchemist.vim           typescript-vim          vim-force.com           vim-lsc
ctrlp.vim               vim-abolish             vim-fugitive            vim-mix-format
dart-vim-plugin         vim-colors-solarized    vim-gitgutter           vim-prettier
delimitMate             vim-elixir              vim-gutentags           vimproc.vim
php.vim                 vim-endwise             vim-js-pretty-template
tsquyomi                vim-flagship            vim-jsx-pretty
```

Some interesting tidbits from my vimrc:
* `set number` and `set relativenumber` - I've found this to let me navigate the fastest, although it is annoying for anyone pairing with you. Luckily you can just turn it off, which any reasonable person should do while pairing :)
* `map <C-Z> :shell` - disarm ctrl-z
* `set ttyfast` - improves redraw speed
* `nnoremap <C-n> :bn<CR>` and `nnoremap <C-p> :bp<CR>` is nice for quickly switching buffers
* `let g:mix_format_on_save` - autoformat elixir files - I like this, but for some reason I hate it when javascript files are autoformatted, so I manually run `:Prettier` for them

## Tmux

I can't believe I stuck with screen for so long. Tmux is great. I use [this guy's tmux config](https://github.com/gpakosz/.tmux) with very few customizations.

## Dotfile management

I use [dfm](https://github.com/justone/dfm) and it is perfectly fine - it uses symlinks, and can deal with having only certain git-managed files in a directory like `~/.config`. It's good. I have no reason to change.

## Fonts

I used to use Consolas but now I use Firacode.

## Cool .bashrc things

### curltimer

Put this in `~/.curl_stat_format`:

```
    time_namelookup:  %{time_namelookup}\n
       time_connect:  %{time_connect}\n
    time_appconnect:  %{time_appconnect}\n
   time_pretransfer:  %{time_pretransfer}\n
      time_redirect:  %{time_redirect}\n
 time_starttransfer:  %{time_starttransfer}\n
                    ----------\n
         time_total:  %{time_total}\n

```
and then you can do `alias curltimer='curl -w "@$HOME/.curl_stat_format" -o /dev/null -s'` for a nice quick benchmarking tool. Combine this with sed, bc, for loops, etc and make your own `ab` :)

### Elixir with history

`alias iex='iex --erl "-kernel shell_history enabled"'` gives your repl shell history.

## Summary

Wow, I haven't really looked at my dotfiles in a while! This feels like a healthy thing to do every once in a while. Now that I've written down the foundations, any future dotfile-related posts can be a little more bite-sized.
