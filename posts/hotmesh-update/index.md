---
title: "hotMesh update"
slug: "/hotmesh-update"
date: "2010-04-13T13:15:30.000Z"
featured: false
draft: false
tags: ["Tech","community wireless","detroit","digital justice","mesh","openmesh"]
---

Hi,

Things have been extremely busy for me with work, and I haven't been able to spend much time with the community wireless network development.  I have, however, rolled out a couple test networks.

Two of the networks are running quite well.  One is in the south Cass Corridor, the other in North Corktown.  The next big network experiment will be in the Northwest Goldberg neighborhood, as a collaborative project with the Detroit Digital Justice Coalition, the Hush House, and some US Social Forum planners.

In addition to this planned network, I'd be interested in rolling out a smaller mesh (3-4 nearby nodes) in the shorter term, for real-world testing.

I'm eventually going to switch one of these networks to a self-hosted management server, as opposed to the Openmesh dashboard, to make it easier to experiment with router firmware and settings.
