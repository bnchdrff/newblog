---
title: "Vote for FACT Social Justice Challenge projects!"
slug: "/vote-for-fact-social-justice-challenge-projects"
date: "2010-10-12T13:50:32.000Z"
featured: false
draft: false
tags: ["Tech","community wireless","detroit","digital justice","garden sensors","gis","mesh","pomaceous detroit","programming","shamless self promotion","wireless"]
---

The public voting period for this year's FACT Social Justice Challenge ends this Friday!  <a href="http://netsquared.org/projectgallery?quicktabs_1=2#quicktabs-1">Read about how to vote on projects</a> -- unfortunately it's a bit complicated.  I should've written up a grant to improve their website.

There are two interesting Detroit projects, as well as a few other cool ones, that I would like to promote:
<ul>
	<li><a href="http://netsquared.org/projects/pomaceous-detroitfruit-trees">Pomaceous Detroit: Fruit Trees</a></li>
	<li><a href="http://netsquared.org/projects/pomaceous-detroitfruit-trees"></a><a href="http://netsquared.org/projects/detroit-garden-sensor-development">Detroit Garden Sensor Development</a></li>
	<li><a href="http://netsquared.org/projects/detroit-garden-sensor-development"></a><a href="http://netsquared.org/projects/solar-powered-computer-project-jungle">Solar-Powered Computer Project in the Jungle</a></li>
	<li><a href="http://netsquared.org/projects/solar-powered-computer-project-jungle"></a><a href="http://netsquared.org/projects/mediamobilizingorg-improving-and-develop">Mediamobilizing.org - Improving and Developing Drupal Modules for Social Media Accessibility</a></li>
	<li><a href="http://netsquared.org/projects/mediamobilizingorg-improving-and-develop"></a><a href="http://netsquared.org/projects/cell-phone-literacy-tool-kit">Dialed In: A Toolkit to Liberate Your Cell Phone and Reclaim Media Agency</a></li>
</ul>
Thanks for voting!
