---
title: "Springtime meshes"
slug: "/springtime-meshes"
date: "2011-04-05T23:19:30.000Z"
featured: false
draft: false
tags: ["Tech","community wireless","detroit","digital justice","mesh","omnicorpdetroit","sxsw"]
---

It has been a busy winter for me, and I'm sad to say that i haven't been very active with the community internet building until recently... that said, it has been pretty awesome for the past few weeks.

[caption id="attachment_278" align="alignleft" width="300" caption="Cut coax, foreground; jig, background"]<img class="size-medium wp-image-278 " title="cut coax, foreground, with jig, background" src="https://old.detriot.org/wp-content/uploads/2011/04/P1010867-300x200.jpg" alt="cut coax, foreground, with jig, background" width="300" height="200" />[/caption]

For the past couple <a href="http://omnicorpdetroit.com">OCD "open hack night" Thursdays</a>, I've invited fellow wireless hackers to work on neighborhood mesh projects, talk about network protocols, and generally goof off in nerdy subversive ways.  That first Thursday, these two super-smart network engineer types, <a href="http://pkmtech.net">Patrick</a> and <a href="http://adamkaminski.com/">Adam</a>, came by.  The next week, piles of coaxial cable showed up and we started building omni antennas!  Also present this past Thursday was <a href="http://iheartryan.com">Ryan Hughes</a>.  He's organizing a community wireless project in Ann Arbor, and recently attended the <a href="http://battlemesh.org/HomePage">Battle Mesh</a>.  Finally, another superleet hacker came into the mix, <a href="http://mike.mg2.org/">Marky B</a>, who is interested in experimenting with mobile mesh devices.

Over the next few weeks, we'll be busy working on these antennae as well as some other experiments.  I've got a few new dual-radio open-mesh nodes on my workbench, and i'd like to work on devising a benchmark test for small neighborhood mesh networks.

[caption id="attachment_281" align="alignright" width="300" caption="my neighborhood: institutions, current mesh, future mesh"]<img class="size-medium wp-image-281 " title="my neighborhood: institutions, current mesh, future mesh" src="https://old.detriot.org/wp-content/uploads/2011/04/4-noco-future-300x279.png" alt="my neighborhood: institutions, current mesh, future mesh" width="300" height="279" />[/caption]

Also notable:  A few weeks ago, <a href="http://schedule.sxsw.com/events/event_IAP7413">I took part in a panel at SXSW Interactive</a> (a surprisingly well-recorded audio feed of the event is available at that link).  Along with Adriel Thornton, Diana Nucera, Jenny Lee, and Invincible, I spoke about Detroit's future media-based economy.  I introduced the idea of "media miles" as a technological parallel to "food miles" — sure, we all like avocados and parmesan reggiano once in a while, but shouldn't we prioritize what we can grow in the back yard?  How can we design technology that emphasize local community exchange, in the same way we are designing food distribution systems that do the same?  Of course, you know the answer... mesh networks and community internet.  I'll be elaborating on these ideas sooner or later, in cahoots with <a href="http://detroitprojectarchive.com">Nina Bianchi</a>.
