---
title: "Recent Hub activity"
slug: "/recent-hub-activity"
date: "2008-12-20T00:28:08.000Z"
featured: false
draft: false
tags: ["Bicycles","bikes","business","thehubofdetroit"]
---

This past week, I wasn't able to put many hours in at the bike shop.  In any case, we have been prepping for our holiday bicycle giveaway, and had a big event on Tuesday night.  A group of volunteers from the Jaffe Raitt lawfirm trucked down a load of bicycles to be rehabbed and gifted.  We had a jolly night of bicycle organizing and mechanizing.

We're still waiting on heat, which means that 100% of my life still involves being in 50-60 degree farenheit environments.  I bought a very warm jacket to deal with this.  Standing around fixing bikes != being a bike messenger.  Wardrobe updates abound.
