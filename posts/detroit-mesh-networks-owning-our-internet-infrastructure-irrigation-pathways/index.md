---
title: "Detroit mesh networks - pwning our internet infrastructure irrigation pathways"
slug: "/detroit-mesh-networks-owning-our-internet-infrastructure-irrigation-pathways"
date: "2009-12-24T20:50:00.000Z"
featured: false
draft: false
tags: ["Tech","detroit","digital justice","fonera","jailbreak","mesh","openmesh","wifi","wireless"]
---

Detroit's first wireless mesh networks are imminent:  I am patiently awaiting the arrival of a slew of Foneras and open-mesh pro routers!

I'm currently researching the procedure to jailbreak foneras.  It's been <a href="http://wiki.cuwin.net/index.php?title=Flashing_the_La_Fonera_with_OpenWRT">well-documented by CUWiN</a> and also <a href="http://pauldotcom.com/wiki/index.php/Episode84">pauldotcom</a>.  I was hoping to have some fun soldering things, but it looks like that won't be necessary.  We'll have to wait until installing cantennas for that.

Coming soon: work days and hackathons so we can all learn how to jailbreak these foneras as well as learn some wireless-networking/OSI layer basics.  After that: a prototype mesh network between <a href="http://thehubofdetroit.org">The Hub</a> and a nearby apartment building.  And then.. ?!?!

PS that pauldotcom page is hilarious!  In ur labz steelin ur meth...
