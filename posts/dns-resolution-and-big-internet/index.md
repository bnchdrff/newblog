---
title: "DNS resolution and Big Internet"
slug: "/dns-resolution-and-big-internet"
date: "2009-12-04T04:12:17.000Z"
featured: false
draft: false
tags: ["Tech","dns","google","netneutrality","opendns","yahoo"]
---

Today, Google made its dns resolver service public (8.8.8.8 and 8.8.4.4 - the IP address equivalent to 1-800-call-sam).

From the <a href="http://code.google.com/speed/public-dns/docs/performance.html#provision">Google Public DNS docs</a>:
<blockquote>The complexity of the name selection problem makes it impossible to solve  online, so we have separated the prefetch system into two components: a  pipeline component, which runs as an external, offline, periodic process that selects  the names to commit to the prefetch system; and a runtime component, that  regularly resolves the selected names according to their TTL windows.</blockquote>

<blockquote>The pipeline component synthesizes the names from a variety of sources, mainly the Google web search index and recent Google Public DNS server logs, and ranks them according to benefit. Benefit is determined by a combination of various factors, including hit rate, the cost of a cache miss, and popularity. The pipeline then factors in the TTL cost; re-ranks all the records; and selects all the names from the top of the list until the cost budget is exhausted. The pipeline continuously updates the list of selected names. The runtime component continuously resolves the selected name records and refreshes them according to their TTL settings.</blockquote>
Google's biggest advantage is they have, as usual, the baddest-ass database available of  popular domain names, and thusly, which are worth caching and prefetching.  This is something that low-tier DNS servers (traditionally ones run by ISPs for customer access, ie a Comcast subscriber gets DHCP served some regional Comcast DNS server addresses) are unable to do.  Local DNS servers have a very small sample size for site popularity, and are completely disconnected from what other local dns servers see as being popular.
<h3>My initial reaction to this</h3>
I do envy Google's attempt to keep it simple - OpenDNS is not keeping it simple at all, and giving users control over things that they shouldn't have, ie creating arbitrarily-resolvable names and blacklisting certain dns entries.  Conceivably, Google engineers can figure out some great algorithms to dramatically improve efficiency and reduce the total traffic caused by superflous dns requests.... BUT.... how about asking ISP nameservers to use Google's server as a primary cache?  Why go directly to users and have them tie themselves to it?  This is a very bizarre strategy that Google's employing.
<h3>OpenDNS</h3>
<a href="http://blog.opendns.com/2009/12/03/opendns-google-dns/">OpenDNS's response</a> is hilarious, though:  "When you use Google DNS, you are getting the experience they prescribe. When you use OpenDNS, you get the Dashboard controls to manage your experience the way you want for you, your family or your organization."  Why the hell do people need control over domain names that should be controlled by other people? I understand blacklisting, but you can do that in all sorts of ways.  You don't need to switch dns servers and use some crazy administrative system.

While looking through OpenDNS's site, I came across <a href="http://blog.opendns.com/2009/11/11/opendns-matching-googles-free-airport-wifi-gift-with-free-services-for-the-holidays/">this hilarious little tidbit</a> - can it be any more obvious that OpenDNS is digging deep?  "Hey network admins, you can use our free service, for free, but only during the holidays! Can I pleeeeease get authority over your dns? Pleeeeease?"

http://serverfault.com/questions/90725/are-ip-addresses-trivial-to-forge

http://code.google.com/speed/public-dns/docs/performance.html#provision

http://blog.opendns.com/2009/12/03/opendns-google-dns/
