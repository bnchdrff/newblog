---
title: "gis brainstorming"
slug: "/gis-brainstorming"
date: "2009-12-19T21:57:43.000Z"
featured: false
draft: false
tags: ["Uncategorized","detroit","drupal","gis"]
---

I've been reading quite a bit recently about gis and mapping with drupal, recently. I've never used drupal to manage a gis system before - only done it with Plone and from-scratch one-offs.  Track my progress at <a href="http://map.archi.st">map.archi.st</a>

I see a lot of need in Detroit for accessible and customizable geographic databases, especially for community groups working in far-flung hoods.  The <a href="http://www.detroitagriculture.org">Detroit Agricultural Network</a> is a perfect example of this:  they support tons of gardens all over the place, and many people may not even know that there's a community garden only a few blocks away from their house or workplace.  Just gotta hook whatever systems up to a shortcode sms gateway....

A good lay-of-the-land overview of drupal gis can be found in <a href="http://lullabot.com/drupal-voices/drupal-voices-67-eric-gundersen-mapping-open-source-government-and-features">this Lullabot podcast</a>

Looks like the Chicago Tech Collective is involved in some drupal gis module development.  Here's one of the projects that resulted:  <a href="http://chicagoancestors.org/">a Chicago library site </a>with a bunch of different datasources squeed into one somewhat useful interface.  Novel!
