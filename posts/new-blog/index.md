---
title: "New blog"
slug: "/new-blog"
date: "2013-10-15T16:03:33.000Z"
featured: false
draft: false
tags: []
---

A few months ago I took down my old blog, in anticipation of building some sort of crazy software to replace it. Then [a ghost appeared](https://github.com/TryGhost/Ghost), and I decided to use this in the meantime.  It isn't the static-file-generating XMPP-backed mesh-network-enhanced wunderkind that I truly desire, but it does indeed work very well as a... blog.

I've been busy. Check out my new lovely project-child, [Detroit Ledger](http://detroitledger.org). I'll be talking about it next week at the Awesome Summit.
