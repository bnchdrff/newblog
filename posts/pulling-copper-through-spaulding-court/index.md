---
title: "Pulling copper through Spaulding Court"
slug: "/pulling-copper-through-spaulding-court"
date: "2011-08-02T15:20:55.000Z"
featured: false
draft: false
tags: ["Uncategorized"]
---

<a href="http://www.flickr.com/photos/28827952@N06/5950335335/in/set-72157627099437509/"><img class="     alignright" title="Cat5 cables, sepertine in the grass outside Spaulding Court" src="https://farm7.static.flickr.com/6024/5950335335_81ba8dc2f2_z.jpg" alt="Cat5 cables, sepertine in the grass outside Spaulding Court" width="86" height="129" /></a>

Images of an Amish barn-raising came to mind while I sat deep within Spaulding Court's westernmost basement, slowly feeding strand after strand of blue wire through plastic conduit. I've never helped raise a barn, but I've seen pictures: a family in the village needs help putting up a gigantic building so all the neighbors come to lend a hand. But on this hot Sunday in July, our barn was less 2x4s and more Cat5 communications wiring. Spaulding Court, the venerable community-owned stone-walled apartment complex, was getting ten high-speed computer networking lines installed by a dozen volunteers.

[caption id="" align="alignleft" width="269" caption="Neighbors and visitors listen to Jon give the lowdown"]<a href="http://www.flickr.com/photos/28827952@N06/5950336477/in/set-72157627099437509"><img class="   " title="Neighbors and visitors listen to Jon give the lowdown" src="https://farm7.static.flickr.com/6143/5950336477_8afb584b34_z.jpg" alt="Neighbors and visitors listen to Jon give the lowdown" width="269" height="179" /></a>[/caption]

The goal of that afternoon's work was to provide each of the south side Court apartments with an ethernet connection to a central router. The occupied units already had wireless internet connections, but these new wires would be more reliable and faster. Getting a cable into each unit took planning: each successive unit needed a cable that would reach out of its junction box to the end unit's box. Volunteers had been measuring the cable out at the previous Soup at Spaulding event.

[caption id="attachment_310" align="alignright" width="225" caption="The first and longest line gets pulled through the conduit"]<a href="https://old.detriot.org/wp-content/uploads/2011/08/internet-tubes.jpg"><img class="size-medium wp-image-310" title="The first and longest line gets pulled through the conduit" src="https://old.detriot.org/wp-content/uploads/2011/08/internet-tubes-225x300.jpg" alt="The first and longest line gets pulled through the conduit" width="225" height="300" /></a>[/caption]

At first I could hear voices from the other units through the plastic conduit—each unit's basement needed a helping hand to push and prod the slowly-thickening bundle of cable as it was pulled through by a lead line—but as the bundle thickened the voices disappeared and we had to rely on cell phones and messengers to communicate. And soon enough, I got word from the grapevine that the final unit had gotten its length, and it was over! We climbed back into the Sunday afternoon heat.

I'll put up an announcement on this blog when we are ready to install the router that links all these cables together.

Check out the latest zine from the Detroit Digital Justice Coalition for a synopsis of all our city's community wireless projects along with a bunch of other relevant news: <a href="http://detroitdjc.org/2011/06/30/ddjc-zine-3-out-now/">http://detroitdjc.org/2011/06/30/ddjc-zine-3-out-now/</a>
