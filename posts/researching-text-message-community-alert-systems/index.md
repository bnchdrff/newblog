---
title: "Researching text message community alert systems"
slug: "/researching-text-message-community-alert-systems"
date: "2011-04-20T16:18:39.000Z"
featured: false
draft: false
tags: ["Uncategorized","cellphones","community safety","community wireless","digital justice"]
---

Preface: I have to put this into a bit of current events context:  I spent the morning researching for this blog post, but was interrupted with news of an <a href="http://news.cnet.com/8301-17938_105-20055431-1.html">ACLU investigation into Michigan police officers breaking into citizen's cell phones</a> and the <a href="http://radar.oreilly.com/2011/04/apple-location-tracking.html">iPhone logging location data and mirroring it to your computer's hard drive</a>.  Both of these stories made me queasy, especially in relation to all of the potentially democratic and liberating ways we can use mobile devices.

So anyways, my neighbors are interested in starting up a community alert system.  We could do it old-school style with a phone/sms tree, but that can introduce time delays, and for rapid response to problems in the neighborhood that's just unacceptable.  In my short conversations with my neighbors so far, people are definitely interested in a voice and text system, but that might be hard to pull off.  I'm interested in how <a href="http://www.villagedefense.com/residential.html">Village Defense</a> works, and whether a staffed call center makes a difference in emergency response situations.

To start things off in my neighborhood, I'm advocating for a SMS mailinglist system.  Initially I was thinking that I could roll something together using twilio and custom code, but I've recently gotten hooked on <a href="http://www.frontlinesms.com/">FrontlineSMS</a>.  I originally came across this project through my tree mapping experiments with <a href="http://ushahidi.com">Ushahidi</a>.  FrontlineSMS uses a GSM modem (either a dedicated device or a tethered cellphone) to send and receive text messages programmatically.  It's written in Java, field-tested, and seems super-solid overall.

The software's GUI lets beginners easily define workflows for all sorts of sms communications needs.  I was excited to discover (via <a href="http://twitter.com/FrontlineSMS/status/60714932668338176">frontlinesms's superfast twitter reply to me</a>) that <a href="http://www.frontlinesms.com/2009/03/09/neighbourhood-watch-frontlinesms-style/">a project in Trinidad and Tobago</a> uses FrontlineSMS for a neighborhood alert system!

It has the option to tie into an online SMS service provider (like clickatell) instead of using the local GSM device, but for this project I like the idea of keeping as much of the infrastructure local as possible.  Eventually, I'd love to experiment with using FrontlineSMS with an alternative cellphone network set up with OpenBTS and a bunch of abandoned old gsm handsets.

Supplies needed:
<ul>
	<li>a computer that can run the FrontlineSMS java app</li>
	<li>a gsm modem, btw $50-100. <a href="http://www.amazon.com/Sierra-Wireless-Mercury-885-Unlocked/dp/B001JU2RRY/">http://www.amazon.com/Sierra-Wireless-Mercury-885-Unlocked/dp/B001JU2RRY/</a> seems to be the only option available via amazon.com; more research pending</li>
	<li>friendly neighbors that have cellphones with SMS capability</li>
	<li>a few organizers that get to know the system really well and advocate for it in the neighborhood</li>
	<li>a loving soul that will donate power and space for the computer to use and inhabit</li>
</ul>
I'm going to take my experimentations with me to a neighborhood meeting tomorrow, and we'll see where everything goes.
