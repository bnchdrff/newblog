---
title: "Lyme disease"
slug: "/lyme-disease"
date: "2010-08-01T04:17:04.000Z"
featured: false
draft: false
tags: ["Tech","journal"]
---

Well, it's Maker Faire weekend and after attending a somewhat lackluster and under-attended "can do camp" in Detroit Proper this past thursday, I'll visit the actual Faire (in Dearborn) on Sunday.  I'm excited to hear <a href="http://www.warpspeed.com">Dewayne Hendricks</a> speak, and can't wait to see some metro-area residents come out of the woodwork.

This coming week, I'll be helping Jenny Lee of <a href="http://alliedmedia.org">Allied Media Projects</a> put together a community wireless network care-package for some Albuquerque community groups.  It's exciting to see our little mesh nodes travel so far!  I'm teaching Jenny not only the mesh setup (using the <a href="http://open-mesh.com/store/">openmesh dashboard</a>) but also the procedure for jailbreaking and flashing our nonstandard equipment.

After a bit of internal deliberation, I've decided to join Detroit's hackerspace, <a href="http://omnicorpdetroit.com">OmniCorpDetroit</a>.  I want to be a part of a nerd club, basically.  Plus, it's a great place to store my SparcStation 20 and VT-420 terminal.  Maybe I can teach someone about baud rates.  PS: I just helped out with their blog/website and it's progressing very well.  The logo is radical.

Finally, during a recent trip to the eastern seaboard, I seemed to have contracted Lyme Disease, and have started a <a href="http://en.wikipedia.org/wiki/Doxycycline">regiment of antibiotic</a> to stymie the lyme.  Why hasn't modern medicine made me invulnerable to this?!?! We should be giving ticks the chronic fatigue and nauseating headaches, not vice-versa.

Well, there goes a perfectly good blog post.  Sorry you had to read about my bacterial infection.  Kids, watch out for ticks.
