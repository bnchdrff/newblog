---
title: "Minimal-ish Drupal hosting with Docker"
slug: "/minimal-ish-drupal-hosting"
date: "2014-02-08T23:03:48.000Z"
featured: false
draft: false
tags: ["nginx","sysadmin"]
---

tl;dr: [Drupal Docker container with nginx, php5-fpm, and linked MariaDB](https://raw.github.com/bnchdrff/drupal-docker)

A few years ago, when I first needed to deploy many Drupal sites, I wrote my own nginx / php5-fpm deployment scripts. What a pain that was: handling Drupal's routes within nginx was a lot of work, and there were always little bugs popping up as projects grew.

I found myself config-stalking some other Drupal sysadmin practitioners: [perusio](https://github.com/perusio/drupal-with-nginx) and [omega8cc](https://github.com/omega8cc/nginx-for-drupal). After getting sick of dancing around multiple levels of config file merges and maintaining my own custom deployment system, I bit the bullet and started using Omega8's barracuda/octopus Aegir setup. It works quite well in production, but Aegir is heavy and fickle.

While I will probably continue to use Omega8's stack for [most of the production work that I launch via Work Dept](http://theworkdept.com), I desire more control for somet other work I'm up to. Some projects need a bit more flexibility than what what the Aegir stack can offer, so I'm [beginning to use Docker containers to manage and isolate components](https://github.com/bnchdrff/drupal-docker). I'll put it in the Docker index once I generalize things and test a bit more.


For nginx and PHP, I've copied some of Omega8's nginx and php-fpm configuration. I may eventually use perusio's configuration if it makes sense. For mysql, I'm using [tianon/mariadb](https://index.docker.io/u/tianon/mariadb/): this is the "minimal-ish" part... I'm building this project to distribute across servers more easily. Eventually I might try linking the php-fpm pools, as well, so they can be load balanced.

I'll post an update when I have a good case study of docker dev-staging-live workflow with a Drupal project.
