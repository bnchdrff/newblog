---
title: "All sorts of hack-job IT work goin on"
slug: "/all-sorts-of-hack-job-it-work-goin-on"
date: "2009-11-29T18:16:28.000Z"
featured: false
draft: false
tags: ["Tech","hub","sysadmin"]
---

So I'm Hubbin it up right now.. setting up an office server that will do filesharing at first, and then be an <a href="http://www.openbravo.com">OpenBravo</a> ERP/POS system server down the road.  For those not in the know, OpenBravo is an awesome product for small businesses that need solid point-of-sale software.  It's done very well with The Hub, and we'll eventually have two POSes networked via this new server - one for the retail counter and another for doing inventory and possibly to handle sales as well.  I also set up a new wireless router running DD-WRT which will improve network reliability.

Just heard that <a href="http://510pen.org">510pen</a> will be attending to the <a href="http://alliedmediaconference.org">Allied Media Conference</a> this year, and they want to help set up a mesh network in Detroit!  They seem to have a nice system figured out.  It's based on the <a href="http://robin-mesh.wik.is/">ROBIN router firmware</a> and <a href="https://www.open-mesh.com/store/products.php?product=Professional-Mini-Router">this nifty little router</a> that can even run with power-over-ethernet... so much fun!
