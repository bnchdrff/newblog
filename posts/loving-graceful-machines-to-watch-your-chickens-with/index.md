---
title: "Loving, Graceful Machines to Watch Your Chickens With"
slug: "/loving-graceful-machines-to-watch-your-chickens-with"
date: "2012-01-09T02:51:24.000Z"
featured: false
draft: false
tags: ["Tech","arduino","chickens","community wireless","garden sensors","http","jeenode","node.js","programming","rf12","tcl","wireless"]
---

It's gotten cold in Detroit again, and the chickens need the usual TLC: more food, more straw, heated water dispensers, and active heating inside the coop. In previous years we've used a single incandescent lightbulb to keep the coop at a hospitable temperature, but this year, we're trying something different: we'll use an infrared heat lamp along with a daylight-simulating incandescent lamp. The heat lamp can stay on all night and not bother the birds -- inconsistent lighting might have a bad impact on the birds' health. The incandescent lamp should turn on at the pre- and post-crepuscular hours at dusk and dawn to add a bit of daylight and keep egg production up.

You may remember last year when I posted about the chicken sensor, an arduino with a temperature sensor and an ethernet shield that attached to a wireless mesh node. After the weather got warm enough, I decommissioned the chicken sensor and haven't touched the arduino platform since. Now it is time for me to jump back into the open source embedded systems fray: either I hack together a system to toggle these lamps and monitor temperature, or my neighbors and I are going to have to run out to the coop during those crepuscular hours and fumble around with electrical switches while getting our hands pecked off by feisty young hens.

So, in the interest of avoiding bloody pecked-up hands, I've planned out an upgraded chicken sensor: one could call it v0.2.0 or one could call it by the codename <a href="http://www.brautigan.net/machines.html#28">"All Watched Over By Machines of Loving Grace."</a> I chose this codename because it describes how the chickens will feel.
<h2>Remote Control</h2>
<a href="https://old.detriot.org/wp-content/uploads/2012/01/jeenode-playin.jpg"><img class="size-medium wp-image-334 alignright" title="JeeLink (left) and JeeNode (on breadboard, right) with FTDI (hanging off the side)" src="https://old.detriot.org/wp-content/uploads/2012/01/jeenode-playin-300x225.jpg" alt="JeeLink (left) and JeeNode (on breadboard, right) with FTDI (hanging off the side)" width="300" height="225" /></a>This year, instead of using an Arduino with Ethernet wired to a mesh node that posts directly to Pachube, I'm going to use a JeeNode that polls a computer in my house that handles all of the posting. The JeeNodes are very cool, and JeeLabs is doing all sorts of neat things with physical computing.

I'm glad that I have taken my time with the JeeNode: while searcing through <a href="http://jeelabs.net/projects/cafe/wiki">the wiki</a> I came across a few shiny gems of projects. The shiniest by far is JeeMon. I was initially planning on writing a little node.js server to listen for temperature packets and post them somewhere on the internet, but <a href="http://jeelabs.net/projects/cafe/wiki/jeemon">JeeMon</a> is a much more full and exciting solution... and an excuse to improve my tcl skills (I've done some quick expect scripts before but that's it). This blog post at jeelabs has a good <a href="http://jeelabs.org/2011/11/25/jeemon-for-early-birds/">introduction and quickstart</a>.

So far, JeeMon seems like a winner of a platform for DIY network physical computing projects. It's backed by a great programmer who develops great hardware. Tcl has some nice text processing features -- so much of the work in this type of project is sucking information out of sometimes-garbled strings. Its event-driven-ness makes sense with random bursts of radio-transmitted information. Finally, although it has a lot of examples dealing with the jeenode world of things, it is not at all tied to a specific platform or network technology.
<h2>Toolchains</h2>
The Arduino IDE is very, very integrated: all of the required libraries for compiling are included within the distribution. This makes it nice and easy for people to get started with Arduino, and it also makes it easier to run multiple virtual environments of different versions of the IDE. It has some built-in features to let you use your own text editor, but it's still pretty clumsy. For past projects I've had to run avrdude with non-standard options to flash a device; I ended up finding where the IDE would put the compiled .hex file during the build and upload it manually. This was a pain.

I started looking around the internet for alternative build methods for arduino programs and didn't really come up with much; there are a lot of one-off projects (see links below) but all of them rely on a forked version of the Arudino libraries, or even a partial set of those libraries. This makes development really tricky, especially if you're including non-standard libraries.

I'd love to see the Arduino IDE expose some of the development processes via the commandline. Another option might be to do an scons setup that uses the arduino IDE's hardware/arduino/boards.txt as a reference.
<h2>What's next</h2>
I'm going to continue tinkering with this stuff. Expect more posts as I learn.

One experiment I'm anxious to try out is to hook a JeeLink to an OpenWRT router and run jeemon right from the router. This might make a good addition to the community wireless toolkit we're building at The Work Department.
<h2>Notes</h2>
When using the arduino sdk to push code to a jeenode, you generally need to set serial.debug_rate to 57600 in your ~/.arduino/preferences.txt

Check your soldering on the jeenode using the voltages listed in the link below. I messed up the RST -&gt; capacitor connection which caused unreliable uploads but didn't interfere with anything else... kinda hard to track that one down :)
<h2>Links</h2>
<h3>Toolchain (your mileage may vary)</h3>
"create your own arduino ide!": <a href="http://pragprog.com/magazines/2011-04/create-your-own-arduino-ide">http://pragprog.com/magazines/2011-04/create-your-own-arduino-ide</a>

forum topic about makefiles: <a href="http://www.arduino.cc/cgi-bin/yabb2/YaBB.pl?num=1284144128">http://www.arduino.cc/cgi-bin/yabb2/YaBB.pl?num=1284144128</a>

arduino-makefile fork on github: <a href="https://github.com/pix/arduino-makefile">https://github.com/pix/arduino-makefile</a>

fix math.h compile error: <a href="http://forums.reprap.org/read.php?146,107925,107925,quote=1">http://forums.reprap.org/read.php?146,107925,107925,quote=1</a> (in short, add an #undef round before round is defined in avr's math.h)

oh, this is nice: <a href="http://www.tmpsantos.com.br/en/2010/12/arduino-uno-ubuntu-cmake/">http://www.tmpsantos.com.br/en/2010/12/arduino-uno-ubuntu-cmake/</a>
<h3>JeeNode</h3>
check board voltages against these values: <a href="http://forum.jeelabs.net/comment/3728#comment-3728">http://forum.jeelabs.net/comment/3728#comment-3728</a>

random weird project: <a href="http://engin1000.pbworks.com/w/page/37615506/RF%20Locator">http://engin1000.pbworks.com/w/page/37615506/RF%20Locator</a>
<h3>Thermometers</h3>
library to read DS1820 sensors: <a href="http://milesburton.com/Dallas_Temperature_Control_Library#Example">http://milesburton.com/Dallas_Temperature_Control_Library#Example</a>

a nice DS18B20 setup that I'm using: <a href="http://www.makershed.com/Waterproof_DS18B20_Digital_Temperature_Sensor_Ex_p/mkad37.htm">http://www.makershed.com/Waterproof_DS18B20_Digital_Temperature_Sensor_Ex_p/mkad37.htm</a>
<h3>JeeMon</h3>
home: <a href="http://jeelabs.net/projects/cafe/wiki/jeemon">http://jeelabs.net/projects/cafe/wiki/jeemon</a>

intro: <a href="http://jeelabs.org/2011/11/25/jeemon-for-early-birds/">http://jeelabs.org/2011/11/25/jeemon-for-early-birds/</a>
