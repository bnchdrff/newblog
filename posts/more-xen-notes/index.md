---
title: "more xen notes"
slug: "/more-xen-notes"
date: "2009-03-13T02:17:00.000Z"
featured: false
draft: false
tags: ["Tech","sysadmin","xen"]
---

debian lenny xen has some quirks - after the hvc0 tty setup drama, i've run into another hiccup:

console access will hang, unless you specify tty1 as the console in the "extras".  so now extras look like:

<pre>extra="clocksource=jiffies xencons=tty1 console=tty1"</pre>
