---
title: "Upcoming DISCOTECH"
slug: "/upcoming-discotech"
date: "2010-05-19T20:40:46.000Z"
featured: false
draft: false
tags: ["Tech","detroit","digital justice"]
---

Discover Technology at the DiscoTech ... Saturday Jun 12 2-5pm @ MOCAD ... Brought to you by the Detroit Digital Justice Coalition .... Own technology, together ... <a title="DiscoTech" href="https://old.detriot.org/wp-content/uploads/2010/05/discotech_2010.pdf">Download PDF of flyer</a> ...

<a href="https://old.detriot.org/wp-content/uploads/2010/05/discotech_2010.png"><img class="alignnone size-full wp-image-151" title="discotech_2010" src="https://old.detriot.org/wp-content/uploads/2010/05/discotech_2010.png" alt="" width="600" height="777" /></a>
