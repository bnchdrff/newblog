---
title: "Typesetting the web"
slug: "/typesetting-the-web"
date: "2010-07-27T02:44:49.000Z"
featured: false
draft: false
tags: ["Asides","Tech","programming","webdev","webfonts"]
---

I've been reading the backlog of the <a href="http://coding.scribd.com/">Scribd team's programming blog</a> and am thoroughly amazed.  For some reason, I'd blindly assumed that flash was being used for a lot of this stuff, but they've been html5-ifying and css3-izing their game.

Check <a href="http://www.fontsquirrel.com/fontface/generator">http://www.fontsquirrel.com/fontface/generator</a> and <a href="http://paulirish.com/2009/bulletproof-font-face-implementation-syntax/">http://paulirish.com/2009/bulletproof-font-face-implementation-syntax/</a>
