---
title: "Tech status update"
date: "2021-04-07T20:41:17.360Z"
featured: false
draft: false
tags: []
description: Switched blogs again, etc.
---

Hi, I have not blogged in quite a while! I'm still here.

What's happened since I last wrote... well I got married, moved to Baltimore, was part of a mass layoff at a cryptocurrency project, became a dad, worked at a startup that got acquired by a huge company, and finally decided to learn Elixir, and got a job at a fintech startup.

My old blog was being built by Gatsby, and it straight-up stopped working (unresolvable sharp build errors), so I decided to write a fast, sloppy static site generator in my new favorite language. [Here's the script](https://gitlab.com/bnchdrff/newblog/-/blob/master/build.exs). I'm enjoying Elixirland so far, and am excited to do some more blogging about it + other stuff too.


