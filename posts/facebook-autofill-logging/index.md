---
title: "Browser autofill snooping on facebook.com"
slug: "/browser-autofill-snooping"
date: "2018-12-11T13:46:37.582Z"
featured: false
draft: false
tags: ["security","browsers","javascript"]
---

I haven't had a facebook account for a few years now -- i did the deletion thing a while ago -- but through a combination of neglect and indifference I've never purged either my Chrome or Firefox saved passwords for the account. Recently, I have had to visit facebook.com more often, and while I was looking through the browser dev tools and noticed that my autofilled email was being phoned home!

![total information awareness](./creepin.png)

Why does my browser allow this to happen? I'd always assumed that the autofill data was sandboxed in some way. Well, in Chrome it is, but not in Firefox. What a bummer. I came across [this Stackoverflow discussion](https://stackoverflow.com/questions/35049555/chrome-autofill-autocomplete-no-value-for-password) that goes into more detail.

It's not often that I find a situation where Firefox really disappoints me; this is one of them.

Even though Chrome's approach isn't perfect (see the stackoverflow thread for examples of workarounds), it's at least a best effort to protect people using the autofill functionality. I know, the nerdiest and most security-consious users will not use autofill at all -- good for you, but you're in the minority, and I think browsers should have sensible pro-privacy defaults. In this case, Chrome shows that a browser can offer the user experience people might expect (autofilled forms) but also provide privacy as well.
