---
title: "Long-distance relationships: hotter mesh"
slug: "/hotter-mesh"
date: "2015-02-17T20:29:00.000Z"
featured: false
draft: false
tags: ["community wireless","mesh","openwrt","batman-adv"]
---

As the result of a recent career shift that I'll address in a future post, I've been able to get back into some wireless tinkering. It turns out to be an activity that pairs well with renovating an old house.

My immediate needs
---

While my previous neighborhood mesh deployment had a pretty dense layout (many 100-200 foot links in all directions) with a requirement for a blanket of wifi access, I'm now more interested in connecting a few houses that are blocks apart, all along the same street. As the space between houses is unoccupied, but the size of the network will ebb and flow along the street as time goes own, this is a good time to try setting up a mesh backhaul.

There isn't a dire need for internet connectivity amongst my neighbors -- while there are a few households that could use a better internet uplink, it's not pressing. I've got some time to figure out the access point / gateway end of things. My priorities at this point are as follows:

* Build a reliable and easily extensible back alley backhaul so I can have an intranet with my neighbors
* Experiment with distributed messaging systems & sensor nets
* Try out various mesh routing systems
* Learn how to better automate deployment along the way

My little testbed network
---

I've deployed three nodes so far -- two picostations near the back alley of two houses about 1/4mi apart, and my desktop computer that meshes with one of those picostations. The picos are running OpenWRT Barrier Breaker, and my desktop runs Ubuntu.

Installation on both openwrt and ubuntu was easy, aside from [a small dependency bug in the openwrt package](https://dev.openwrt.org/ticket/14452). I definitely prefer having batman-adv manage the mesh on layer 2 in the case of setting up a backhaul network like this -- it's a lot easier to just do manual IP addressing. This theme of choosing simplicity & explicit configuration over the complexity of over-automation will continue, I think.

I am now realizing that I never posted about the <a href="https://github.com/bnchdrff/breakafon">helper scripts</a> I wrote a long time ago to assist with jailbreaking Foneræ. This is about as sophisticated as things got, in the past. I'd like to survey other systems for speeding up community wireless deployments, like the [Freifunk Image Builder](http://imagebuilder.augsburg.freifunk.net/).


More general ideas
---

I don't think it's a good idea to try and develop a plug-and-play community wireless network, yet: none of the clearly-defined use cases are easily solvable by the technology (hardware & software) that is available to low-budget community groups.

In my own networking work (and even tech/IT/programming work in general), I usually develop a personal arsenal of tools and patterns that I can implement and re-implement as needed. My arsenal might be a little different than someone else's because my background is different, but we still might be able to share componenets.

In the land of community wireless networks, there are many tinkerers like me all over the world, and we do try to share bits and pieces of our toolkits. Sometimes it's useful, sometimes it isn't. To me, the most exciting aspect of the maturation and growth of community wireless network technology is the potential for re-implementable components On the software side, using OpenWRT packages is a clear winner. For configuration... I'm really not sure what to do. For hardware, I have no clue.

To be continued.
