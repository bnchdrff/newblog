---
title: "Detroit digital justice-related netsquared/FACT grant ideas"
slug: "/detroit-digital-justice-related-netsquaredfact-grant-ideas"
date: "2010-10-05T15:45:35.000Z"
featured: false
draft: false
tags: ["Tech","detroit","digital justice"]
---

I worked with two groups in applying for Netsquared/FACT grants:

<a href="http://netsquared.org/projects/detroit-garden-sensor-development">http://netsquared.org/projects/detroit-garden-sensor-development</a>

<a href="http://netsquared.org/projects/pomaceous-detroitfruit-trees">http://netsquared.org/projects/pomaceous-detroitfruit-trees</a>

The former project will hopefully lead to other sensor tech developments including environmental air-quality sensors.

The latter is a bit more ethereal but will hopefully get more urban agriculture-minded people interested in using media for organizing.

If you have a moment, leave a note of support by commenting and "like"ing the projects.
